class CreateSliders < ActiveRecord::Migration[5.2]
  def change
    create_table :sliders do |t|
      t.string :title
      t.string :subtitle
      t.integer :enum_social_type

      t.timestamps
    end
  end
end
