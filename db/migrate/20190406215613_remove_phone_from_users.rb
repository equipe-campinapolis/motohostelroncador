class RemovePhoneFromUsers < ActiveRecord::Migration[5.2]
  	def self.up  
  		remove_column :users, :phone 
	end 
end
