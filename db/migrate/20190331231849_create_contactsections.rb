class CreateContactsections < ActiveRecord::Migration[5.2]
  def change
    create_table :contactsections do |t|
      t.references :contact, foreign_key: true
      t.string :title
      t.text :subtitle

      t.timestamps
    end
  end
end
