class Room < ApplicationRecord
    attr_accessor :remove_primary_image,:remove_images
    attribute :remove_images, :json
    has_many :roomsections, :dependent => :destroy, :inverse_of => :room
    has_many_attached :images
    accepts_nested_attributes_for :roomsections, :allow_destroy => true
	enum enum_booking_type: {"por pessoa":1,"por noite":2}
	after_save :delete_images, if: -> { remove_images.present? }

    validates :images, presence: true
	
	def delete_images
		images.each_with_index { |_, index| images[index].purge if remove_images.include?(index.to_s) }
	end

	after_save do
		primary_image.purge if remove_primary_image == '1'
		Array(remove_images).each do |id|
			images.find_by_id(id).try(:purge)
		end
	end

    rails_admin do
		label " Quartos"
    	label_plural " Quartos"
    	navigation_label 'Pagina Quartos'
    	navigation_icon 'icon-home'
        weight 7
    	edit do
            # include_fields :title,:subtitle,:price,:enum_booking_type,
                       # :images,:tag_list
    		field :title do
    			label "Titulo"
    		end
    		field :subtitle do
    			label "Subtitulo"
    		end
    		field :price do
    			label "preço"
                help "utilize o ponto. Não utilize virgulas por favor" 
    		end
    		field :enum_booking_type do
    			label "tipo"
    		end
    		field :roomsections do
                label "itens da descrição"
            end
            field :images do
    			label "imagens"
    		end
            #configure :tag_list do
            #    ratl_max_suggestions -1
            #    label 'itens da descrição'
            #    help 'separe os itens por virgula'
            #end
    	end
    	list do
        field :title do
                label "Titulo"
            end
            field :subtitle do
                label "Subtitulo"
            end
            field :price do
                label "preço" 
            end
            field :enum_booking_type do
                label "tipo"
            end
            field :images do
                label "imagens"
            end
            field :roomsections do
                label "item descritor"
            end
    	end
  	end

end
