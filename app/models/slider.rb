class Slider < ApplicationRecord
	has_one_attached :image
	enum enum_social_type:{"Facebook":1,"Instagram":2}

    validates :image, presence: true
    
	rails_admin do
		label " Rede Social"
    	label_plural " Redes Sociais"
    	navigation_label 'Pagina Inicio'
    	navigation_icon 'icon-heart'
    	weight 2
    	edit do
    		field :title do
    			label "Titulo"
    		end
    		field :subtitle do
    			label "subtitulo"
    		end
    		field :enum_social_type do
    			label "rede social"
    		end
    		field :url do
    			label "endereço da rede social"
    		end
            field :image do
                label "imagem"
            end
    	end
    	list do
    		field :title do
    			label "Titulo"
    		end
    		field :subtitle do
    			label "subtitulo"
    		end
    		field :enum_social_type do
    			label "rede social"
    		end
            field :image do
                label "imagem"
            end
    	end
  	end

end
