class User < ApplicationRecord
  devise :database_authenticatable, :rememberable, :validatable


  enum tipo: [:cliente, :admin,:sistema]
  enum status: [:ativo, :inativo]

  def email_required?
    true
  end

  rails_admin do
	  label " Usuário"
    label_plural " Usuários"
    navigation_icon 'icon-user'
    weight 1
    list do
      field :name do
        label "Nome"
      end
      field :tipo
      field :email
      field :status
    end
    edit do
      field :tipo
      field :status
      field :name do
        label "nome"
      end
      field :email do
        label "e-mail"
      end 
      field :password,:password do
        label "senha"
      end
      field :password_confirmation, :password do
        label "confirma a senha"
      end

    end
  end
end
