class ApplicationPolicy
 attr_reader :user, :record
  def initialize(user, record)
    @user = user
    @record = record
  end

  def dashboard?
    tipo = user.tipo.downcase
    true if tipo == "admin" or tipo == "cliente"
  end

  def index?
    tipo = user.tipo.downcase
    true if tipo == "admin" or tipo == "cliente"
  end

  def show?
    tipo = user.tipo.downcase
    true if tipo == "admin" or tipo == "cliente"
  end

  def create?
    tipo = user.tipo.downcase
    true if tipo == "admin" or tipo == "cliente"
  end

  def new?
    tipo = user.tipo.downcase
    true if tipo == "admin" or tipo == "cliente"
  end

  def update?
    tipo = user.tipo.downcase
    true if tipo == "admin" or tipo == "cliente"
  end

  def edit?
    tipo = user.tipo.downcase
    true if tipo == "admin" or tipo == "cliente"
  end

  def destroy?
    tipo = user.tipo.downcase
    true if tipo == "admin" or tipo == "cliente"
  end

  def export?
    tipo = user.tipo.downcase
    true if tipo == "admin" or tipo == "cliente"
  end

  def show_in_app?
    tipo = user.tipo.downcase
    true if tipo == "admin" or tipo == "cliente"
  end

  class Scope
    attr_reader :user, :scope
    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope.all
    end
  end
end
